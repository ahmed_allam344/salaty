package com.example.allam.salaty.fragment;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RadioButton;
import android.widget.TextView;
import android.widget.Toast;

import com.example.allam.salaty.database.DatabaseQueries;
import com.example.allam.salaty.model.Adhan.AdhanData;
import com.example.allam.salaty.model.PrayValue;
import com.example.allam.salaty.R;
import com.example.allam.salaty.utils.Utils;

import java.text.ParseException;
import java.util.Calendar;
import java.util.Date;

/**
 * Created by Allam on 12/11/2016.
 */
public class AdhanFragment extends Fragment {
    private RadioButton mStateRadioButton[][] = new RadioButton[5][3];
    private TextView mAdhanDateTextView[] = new TextView[5];
    private String mPrayerTimes[] = new String[5];
    private static final String EXTRA_DATE = "extra_date";
    String mDate;
    private DatabaseQueries mDatabaseQueries;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

    }

    public static AdhanFragment newInstance(String date) {
        AdhanFragment fragment = new AdhanFragment();
        Bundle b = new Bundle();
        b.putString(EXTRA_DATE, date);
        fragment.setArguments(b);
        return fragment;
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.adhan_fragment, container, false);

        initView(view);

        mDate = getArguments().getString(EXTRA_DATE);
        mDatabaseQueries = new DatabaseQueries(getActivity());

        refreshPrayerTimes();

        // NOTE it brings only the jan month  will updated later
        AdhanData adhanTimes = mDatabaseQueries.getPrayTimesAtDay(mDate);
        if (adhanTimes != null) {
            mPrayerTimes[0] = adhanTimes.getTimings().getFajr();
            mPrayerTimes[1] = adhanTimes.getTimings().getDhuhr();
            mPrayerTimes[2] = adhanTimes.getTimings().getAsr();
            mPrayerTimes[3] = adhanTimes.getTimings().getMaghrib();
            mPrayerTimes[4] = adhanTimes.getTimings().getIsha();
            mPrayerTimes = Utils.formatPrayerTime(mPrayerTimes);

            for (int i = 0; i < 5; i++)
                mAdhanDateTextView[i].setText(mPrayerTimes[i]);

        }

        for (int i = 0; i < 5; i++) {
            for (int j = 0; j < 3; j++) {
                final int row = i, col = j;
                mStateRadioButton[i][j].setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        if (compareTwoDates(mDate)) {
                            Toast.makeText(getActivity(), R.string.lie, Toast.LENGTH_SHORT).show();
                        } else {
                            //get the previous pray values to update on it.
                            PrayValue prayValue = mDatabaseQueries.getPrayValuesAtDay(mDate);
                            if (prayValue == null) prayValue = new PrayValue();

                            if (row == 0) prayValue.setFajr(col);
                            else if (row == 1) prayValue.setDhuhr(col);
                            else if (row == 2) prayValue.setAsr(col);
                            else if (row == 3) prayValue.setMaghrib(col);
                            else prayValue.setIsha(col);
                            prayValue.setDate(mDate);
                            new DatabaseQueries(getActivity()).insertPrayValues(prayValue);
                        }
                    }
                });
            }
        }

        return view;
    }


    private void initView(View v) {

        mStateRadioButton[0][0] = (RadioButton) v.findViewById(R.id.f_not_yet);
        mStateRadioButton[1][0] = (RadioButton) v.findViewById(R.id.d_not_yet);
        mStateRadioButton[2][0] = (RadioButton) v.findViewById(R.id.a_not_yet);
        mStateRadioButton[3][0] = (RadioButton) v.findViewById(R.id.m_not_yet);
        mStateRadioButton[4][0] = (RadioButton) v.findViewById(R.id.i_not_yet);

        mStateRadioButton[0][1] = (RadioButton) v.findViewById(R.id.f_delayed);
        mStateRadioButton[1][1] = (RadioButton) v.findViewById(R.id.d_delayed);
        mStateRadioButton[2][1] = (RadioButton) v.findViewById(R.id.a_delayed);
        mStateRadioButton[3][1] = (RadioButton) v.findViewById(R.id.m_delayed);
        mStateRadioButton[4][1] = (RadioButton) v.findViewById(R.id.i_delayed);

        mStateRadioButton[0][2] = (RadioButton) v.findViewById(R.id.f_in_time);
        mStateRadioButton[1][2] = (RadioButton) v.findViewById(R.id.d_in_time);
        mStateRadioButton[2][2] = (RadioButton) v.findViewById(R.id.a_in_time);
        mStateRadioButton[3][2] = (RadioButton) v.findViewById(R.id.m_in_time);
        mStateRadioButton[4][2] = (RadioButton) v.findViewById(R.id.i_in_time);


        mAdhanDateTextView[0] = (TextView) v.findViewById(R.id.fajr_date);
        mAdhanDateTextView[1] = (TextView) v.findViewById(R.id.dhuhr_date);
        mAdhanDateTextView[2] = (TextView) v.findViewById(R.id.asr_date);
        mAdhanDateTextView[3] = (TextView) v.findViewById(R.id.maghrib_date);
        mAdhanDateTextView[4] = (TextView) v.findViewById(R.id.isha_date);

    }

    private void refreshPrayerTimes() {
        PrayValue previousPrayValue = mDatabaseQueries.getPrayValuesAtDay(mDate);
        if (previousPrayValue != null) {
            mStateRadioButton[0][previousPrayValue.getFajr()].setChecked(true);
            mStateRadioButton[1][previousPrayValue.getDhuhr()].setChecked(true);
            mStateRadioButton[2][previousPrayValue.getAsr()].setChecked(true);
            mStateRadioButton[3][previousPrayValue.getMaghrib()].setChecked(true);
            mStateRadioButton[4][previousPrayValue.getIsha()].setChecked(true);
        }
    }

    private boolean compareTwoDates(String day) {
        if (day == null) {
            return false;
        }
        Calendar cal = Calendar.getInstance();
        String currentDate = Utils.SIMPLE_DATE_FORMAT_DATABASE_QUERY.format(cal.getTime());
        Date date1 = null;
        Date date2 = null;
        try {
            date1 = Utils.SIMPLE_DATE_FORMAT_DATABASE_QUERY.parse(currentDate);
            date2 = Utils.SIMPLE_DATE_FORMAT_DATABASE_QUERY.parse(day);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        //if Fragment day is after the current day prevent him from check.
        return date1.compareTo(date2) < 0;

    }


}
