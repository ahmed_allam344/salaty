package com.example.allam.salaty.service;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;

import com.example.allam.salaty.utils.Utils;

/**
 * Created by shawara on 4/3/2017.
 */

public class AzanSoundBroadcast extends BroadcastReceiver {
    @Override
    public void onReceive(Context context, Intent intent) {
        String azanName=intent.getStringExtra(Utils.EXTRA_AZNA_NAME);
        Utils.pushAzanNotification(context,azanName, "Now adhan of " + azanName);
        Utils.startNextPryerAzan(context);
    }


}
