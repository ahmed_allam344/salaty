package com.example.allam.salaty.database;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

import com.example.allam.salaty.database.AdhanSchema.AdhanTable;
import com.example.allam.salaty.database.AdhanSchema.PrayValueTable;

import static com.example.allam.salaty.database.AdhanSchema.DATABASE_NAME;

/**
 * Created by Allam on 1/4/2017.
 */
public class AdhanDBHelper extends SQLiteOpenHelper {
    private static int VERSION = 2;

    public AdhanDBHelper(Context context) {
        super(context, DATABASE_NAME, null, VERSION);
    }


    @Override
    public void onCreate(SQLiteDatabase db) {
        String CREATE_ATHAN_TABLE = "CREATE TABLE " + AdhanTable.TABLE_NAME + " (" +
                AdhanTable.Columns._ID + " INTEGER PRIMARY KEY," +
                AdhanTable.Columns.FAGR + " TEXT NOT NULL, " +
                AdhanTable.Columns.SUNRISE + " TEXT NOT NULL, " +
                AdhanTable.Columns.DHUHR + " TEXT NOT NULL, " +
                AdhanTable.Columns.ASR + " TEXT NOT NULL, " +
                AdhanTable.Columns.MAGHRIB + " TEXT NOT NULL, " +
                AdhanTable.Columns.ISHA + " TEXT NOT NULL, " +
                AdhanTable.Columns.IMSAK + " TEXT NOT NULL, " +
                AdhanTable.Columns.MIDNIGHT + " TEXT NOT NULL, " +
                AdhanTable.Columns.DATE + " TEXT NOT NULL UNIQUE ON CONFLICT REPLACE," +
                AdhanTable.Columns.TIMESTAMP + " TEXT NOT NULL UNIQUE ON CONFLICT REPLACE ); ";

        String CREATE_PRAY_VALUE_TABLE = "CREATE TABLE " + PrayValueTable.TABLE_NAME + " (" +
                PrayValueTable.Columns._ID + " INTEGER PRIMARY KEY," +
                PrayValueTable.Columns.FAGR_VALUE + " INTEGER NOT NULL, " +
                PrayValueTable.Columns.SUNRISE_VALUE + " INTEGER NOT NULL, " +
                PrayValueTable.Columns.DHUHR_VALUE + " INTEGER NOT NULL, " +
                PrayValueTable.Columns.ASR_VALUE + " INTEGER NOT NULL, " +
                PrayValueTable.Columns.MAGHRIB_VALUE + " INTEGER NOT NULL, " +
                PrayValueTable.Columns.ISHA_VALUE + " INTEGER NOT NULL, " +
                PrayValueTable.Columns.IMSAK_VALUE + " INTEGER NOT NULL, " +
                PrayValueTable.Columns.MIDNIGHT_VALUE + " INTEGER NOT NULL, " +
                PrayValueTable.Columns.DATE_VALUE + " TEXT NOT NULL UNIQUE, " +
                " FOREIGN KEY (" + PrayValueTable.Columns.DATE_VALUE + ") REFERENCES " +
               AdhanTable.TABLE_NAME + " (" + AdhanTable.Columns.DATE + "), " +
                " UNIQUE (" + PrayValueTable.Columns.DATE_VALUE + ") ON CONFLICT REPLACE);";

        db.execSQL(CREATE_ATHAN_TABLE);
        db.execSQL(CREATE_PRAY_VALUE_TABLE);
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        db.execSQL("DROP TABLE IF EXISTS " + AdhanTable.TABLE_NAME);
        db.execSQL("DROP TABLE IF EXISTS " + PrayValueTable.TABLE_NAME);
        onCreate(db);
    }
}
