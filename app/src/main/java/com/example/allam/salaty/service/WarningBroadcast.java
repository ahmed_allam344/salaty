package com.example.allam.salaty.service;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;

import com.example.allam.salaty.utils.Utils;

/**
 * Created by shawara on 4/4/2017.
 */

public class WarningBroadcast extends BroadcastReceiver {
    @Override
    public void onReceive(Context context, Intent intent) {
        String azanName = intent.getStringExtra(Utils.EXTRA_AZNA_NAME);
        String nextAzanName = Utils.getNextAzanName(context, azanName);
        long time = intent.getLongExtra(Utils.EXTRA_TIME, System.currentTimeMillis());

        boolean isWarnign = intent.getBooleanExtra(Utils.EXTRA_IS_WARNING, false);
        String title = "", content = "";
        if (isWarnign) {
            title = nextAzanName + "!!";
            content = "Did you pray " + azanName +
                    " ? there is only " + (Utils.PERIOD_WARNING_CHECK / (1000 * 60)) + " minutes before " + nextAzanName + ".";

            Utils.pushNotification(context, title, content, azanName, time);

        } else {
            title = azanName;
            content = "Did you pray " + azanName + " ?";
            Utils.pushNotification(context, title, content, azanName, time);

        }
        Utils.startAzanNotifications(context);

    }
}
