package com.example.allam.salaty.activity;

import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.Html;

import com.example.allam.salaty.R;
import com.example.allam.salaty.fragment.SettingsFragment;
import com.example.allam.salaty.utils.Utils;

import java.util.Date;

public class SettingsActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getFragmentManager().beginTransaction().replace(android.R.id.content, new SettingsFragment()).commit();

        final ActionBar actionBar = getSupportActionBar();
        actionBar.setTitle(Html.fromHtml("<font color='#534c49'>" + "Settings" + "</font>"));
        actionBar.setHomeAsUpIndicator(R.drawable.back_icon);
        //actionBar.setDisplayHomeAsUpEnabled(true);
        //actionBar.setElevation(4);

    }


}
