package com.example.allam.salaty.database;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.net.Uri;
import android.util.Log;

import com.example.allam.salaty.database.AdhanSchema.AdhanTable;
import com.example.allam.salaty.database.AdhanSchema.PrayValueTable;
import com.example.allam.salaty.model.Adhan.AdhanData;
import com.example.allam.salaty.model.PrayValue;
import com.example.allam.salaty.utils.Utils;

import java.util.ArrayList;
import java.util.List;

import static com.example.allam.salaty.database.AdhanSchema.AdhanTable.Columns.*;
import static com.example.allam.salaty.database.AdhanSchema.AdhanTable.TABLE_NAME;
import static com.example.allam.salaty.database.AdhanSchema.PrayValueTable.Columns.*;


/**
 * Created by Allam on 1/4/2017.
 */
public class DatabaseQueries {
    private static SQLiteDatabase mSQLiteDatabase;
    private static AdhanDBHelper mDBHelper;

    public DatabaseQueries(Context context) {
        mDBHelper = new AdhanDBHelper(context.getApplicationContext());
        mSQLiteDatabase = mDBHelper.getWritableDatabase();
    }

    private ContentValues getAdhanContentValues(AdhanData athan) {
        ContentValues contentValues = new ContentValues();
        contentValues.put(FAGR, athan.getTimings().getFajr());
        contentValues.put(SUNRISE, athan.getTimings().getSunrise());
        contentValues.put(DHUHR, athan.getTimings().getDhuhr());
        contentValues.put(ASR, athan.getTimings().getAsr());
        contentValues.put(MAGHRIB, athan.getTimings().getMaghrib());
        contentValues.put(ISHA, athan.getTimings().getIsha());
        contentValues.put(IMSAK, athan.getTimings().getImsak());
        contentValues.put(MIDNIGHT, athan.getTimings().getMidnight());
        contentValues.put(DATE, athan.getDate().getReadable());
        contentValues.put(TIMESTAMP, athan.getDate().getTimestamp());
        return contentValues;
    }

    private ContentValues getPrayValueContentValues(PrayValue prayValue) {
        ContentValues contentValues = new ContentValues();
        contentValues.put(FAGR_VALUE, prayValue.getFajr());
        contentValues.put(SUNRISE_VALUE, prayValue.getSunrise());
        contentValues.put(DHUHR_VALUE, prayValue.getDhuhr());
        contentValues.put(ASR_VALUE, prayValue.getAsr());
        contentValues.put(MAGHRIB_VALUE, prayValue.getMaghrib());
        contentValues.put(ISHA_VALUE, prayValue.getIsha());
        contentValues.put(IMSAK_VALUE, prayValue.getImsak());
        contentValues.put(MIDNIGHT_VALUE, prayValue.getMidnight());
        contentValues.put(DATE_VALUE, prayValue.getDate());
        return contentValues;
    }


    public long insertAdhanTimes(AdhanData adhanData) {
        ContentValues contentValues = getAdhanContentValues(adhanData);
        long inserted = mSQLiteDatabase.insertWithOnConflict(TABLE_NAME, null, contentValues, SQLiteDatabase.CONFLICT_REPLACE);
        return inserted;
    }

    public long insertPrayValues(PrayValue prayValue) {
        ContentValues contentValues = getPrayValueContentValues(prayValue);
        long inserted = mSQLiteDatabase.insertWithOnConflict(PrayValueTable.TABLE_NAME, null, contentValues, SQLiteDatabase.CONFLICT_REPLACE);
        Log.i(Utils.LOGTAG, inserted + " Pray Values rows inserted");
        return inserted;
    }



    public List<AdhanData> getAllPrayTimes() {
        List<AdhanData> listData = new ArrayList<>();
        DataCursorWraper dataCursorWraper;
        Cursor cursor = mSQLiteDatabase.query(AdhanTable.TABLE_NAME, null, null, null, null, null, null);
        cursor.moveToFirst();
        while (!cursor.isAfterLast()) {
            dataCursorWraper = new DataCursorWraper(cursor);
            listData.add(dataCursorWraper.getAdhanCursorData());
            cursor.moveToNext();
        }
        cursor.close();
        return listData;
    }

    public List<PrayValue> getAllPrayValues() {
        List<PrayValue> listValues = new ArrayList<>();
        DataCursorWraper dataCursorWraper;
        Cursor cursor = mSQLiteDatabase.query(PrayValueTable.TABLE_NAME, null, null, null, null, null, null);
        cursor.moveToFirst();
        while (!cursor.isAfterLast()) {
            dataCursorWraper = new DataCursorWraper(cursor);
            listValues.add(dataCursorWraper.getPrayValuesCursorData());
            cursor.moveToNext();
        }
        cursor.close();
        return listValues;
    }

    public AdhanData getPrayTimesAtDay(String Day) {
        AdhanData adhanData = null;
        Cursor cursor = mSQLiteDatabase.query(AdhanTable.TABLE_NAME, null, DATE + " = ?", new String[]{Day}, null, null, null);
        cursor.moveToFirst();
        if (!cursor.isAfterLast()) {
             adhanData = new DataCursorWraper(cursor).getAdhanCursorData();
        }
        cursor.close();
        return adhanData;

    }


    public PrayValue getPrayValuesAtDay(String Day) {
        PrayValue prayValue = null;
        Cursor cursor = mSQLiteDatabase.query(PrayValueTable.TABLE_NAME, null, DATE + " = ?", new String[]{Day}, null, null, null);
        cursor.moveToFirst();
        if (!cursor.isAfterLast()) {
             prayValue = new DataCursorWraper(cursor).getPrayValuesCursorData();
        }
        cursor.close();
        return prayValue;
    }



}
