package com.example.allam.salaty.utils;


import android.app.AlarmManager;
import android.app.Notification;
import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.media.AudioAttributes;
import android.media.RingtoneManager;
import android.net.ConnectivityManager;
import android.net.Uri;
import android.os.Build;
import android.os.PowerManager;
import android.preference.PreferenceManager;
import android.support.v4.app.NotificationCompat;
import android.support.v4.app.NotificationManagerCompat;
import android.util.Log;
import android.util.Pair;

import com.example.allam.salaty.R;
import com.example.allam.salaty.activity.SalatyActivity;
import com.example.allam.salaty.database.DatabaseQueries;
import com.example.allam.salaty.model.Adhan;
import com.example.allam.salaty.model.PrayValue;
import com.example.allam.salaty.service.AzanSoundBroadcast;
import com.example.allam.salaty.service.PollDateService;
import com.example.allam.salaty.service.DoneBroadcast;
import com.example.allam.salaty.service.WarningBroadcast;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

/**
 * Created by shawara on 1/6/2017.
 */

public class Utils {

    public static long PERIOD_AFTER_CHECK = 1000 * 60 * 25; // 20 minutes
    public static long PERIOD_WARNING_CHECK = 1000 * 60 * 25; // 25 minutes

    public static final String EXTRA_AZNA_NAME = "azan_name";
    public static final String EXTRA_IS_WARNING = "is_warning";
    public static final String EXTRA_TIME = "azan_time";


    public static final SimpleDateFormat SIMPLE_DATE_FORMAT = new SimpleDateFormat("EEE - dd MMM y");
    public static final SimpleDateFormat SIMPLE_DATE_FORMAT_DATABASE_QUERY = new SimpleDateFormat("dd MMM y");
    public static final String LOGTAG = "SalatyTag";
    private static final String PREF_IS_ALARM_ON = "isAlarmOn";

    public static String[] formatPrayerTime(String a[]) {
        for (int i = 0; i < a.length; i++) {
            String s = a[i].substring(2, 5);
            int h = Integer.parseInt(a[i].substring(0, 2));
            String am = (h > 11 ? " pm" : " am");
            if (h > 12) h -= 12;

            if (h < 10) a[i] = "0" + h + s + am;
            else a[i] = h + s + am;
        }
        return a;
    }

    public static Pair<String, Long>[] getPrevAndNextAzanNameAndTime(Context context) {
        Pair<String, Long>[] pairs = new Pair[2];

        Calendar cal = Calendar.getInstance();

        String prevDate = SIMPLE_DATE_FORMAT_DATABASE_QUERY.format(new Date(cal.getTimeInMillis() - 1000 * 60 * 60 * 24));
        String currentDate = SIMPLE_DATE_FORMAT_DATABASE_QUERY.format(new Date());
        String tomDate = SIMPLE_DATE_FORMAT_DATABASE_QUERY.format(new Date(cal.getTimeInMillis() + 1000 * 60 * 60 * 24));

        DatabaseQueries db = new DatabaseQueries(context);
        Adhan.AdhanData adhan = db.getPrayTimesAtDay(currentDate);
        Adhan.AdhanData adhan0 = db.getPrayTimesAtDay(prevDate);
        Adhan.AdhanData adhan2 = db.getPrayTimesAtDay(tomDate);

        Log.d("Utils", "getNextAzanNameAndTime: " + currentDate + " " + tomDate);

        if (adhan == null) return null;

        long[] dates = {
                convertPrayerTimetoLong(adhan0.getTimings().getIsha()) - 1000 * 60 * 60 * 24,
                convertPrayerTimetoLong(adhan.getTimings().getFajr()),
                convertPrayerTimetoLong(adhan.getTimings().getDhuhr()),
                convertPrayerTimetoLong(adhan.getTimings().getAsr()),
                convertPrayerTimetoLong(adhan.getTimings().getMaghrib()),
                convertPrayerTimetoLong(adhan.getTimings().getIsha()),
                convertPrayerTimetoLong(adhan2.getTimings().getFajr()) + 1000 * 60 * 60 * 24
        };

        String name[] = {
                context.getString(R.string.isha),
                context.getString(R.string.fajr),
                context.getString(R.string.dhuhr),
                context.getString(R.string.asr),
                context.getString(R.string.maghrib),
                context.getString(R.string.isha),
                context.getString(R.string.fajr),
        };

        long currentTime = System.currentTimeMillis();
        for (int i = 1; i < dates.length; i++) {
            if (currentTime >= dates[i - 1] && currentTime <= dates[i]) {
                pairs[0] = new Pair<>(name[i - 1], dates[i - 1]);
                pairs[1] = new Pair<>(name[i], dates[i]);
                return pairs;
            }
        }
        return null;

    }


    public static long convertPrayerTimetoLong(String time) {

        String s = time.substring(0, 2);
        String s2 = time.substring(3, 5);
        int t1 = Integer.parseInt(s);
        int t2 = Integer.parseInt(s2);

        Calendar cal = Calendar.getInstance();
        cal.setTime(new Date());
        int day = cal.get(Calendar.DAY_OF_MONTH);
        int month = cal.get(Calendar.MONTH);
        int year = cal.get(Calendar.YEAR);
        cal.set(year, month, day, t1, t2, 0);

        long timestamp = cal.getTimeInMillis();
        return timestamp; // twominutes befor adhan
    }

    public static boolean isNetworkAvailableAndConnected(Context context) {
        ConnectivityManager cm = (ConnectivityManager) context.getSystemService(context.CONNECTIVITY_SERVICE);
        boolean isNetworkAvailable = cm.getActiveNetworkInfo() != null;
        boolean isNetworkConnected = isNetworkAvailable && cm.getActiveNetworkInfo().isConnected();
        return isNetworkConnected;
    }

    public static void setAdhanPrayerOn(Context context, boolean isOn) {
        PreferenceManager.getDefaultSharedPreferences(context.getApplicationContext())
                .edit()
                .putBoolean(PREF_IS_ALARM_ON, isOn)
                .apply();
    }

    public static boolean isAdhanPrayerOn(Context context) {
        return PreferenceManager.getDefaultSharedPreferences(context.getApplicationContext()).getBoolean(PREF_IS_ALARM_ON, false);
    }

    public static void update(String azanName, Context context, long time) {

        DatabaseQueries mDatabaseQueries = new DatabaseQueries(context);
        String mDate = Utils.SIMPLE_DATE_FORMAT_DATABASE_QUERY.format(new Date(time));

        PrayValue prayValue = mDatabaseQueries.getPrayValuesAtDay(mDate);
        if (prayValue == null) prayValue = new PrayValue();

        if (azanName.equals(context.getResources().getString(R.string.fajr))) prayValue.setFajr(2);
        else if (azanName.equals(context.getResources().getString(R.string.dhuhr)))
            prayValue.setDhuhr(2);
        else if (azanName.equals(context.getResources().getString(R.string.asr)))
            prayValue.setAsr(2);
        else if (azanName.equals(context.getResources().getString(R.string.maghrib)))
            prayValue.setMaghrib(2);
        else prayValue.setIsha(2);
        prayValue.setDate(mDate);
        new DatabaseQueries(context).insertPrayValues(prayValue);
    }

    public static String getNextAzanName(Context c, String Name) {
        if (Name.equals(c.getString(R.string.fajr))) return c.getString(R.string.dhuhr);
        if (Name.equals(c.getString(R.string.dhuhr))) return c.getString(R.string.asr);
        if (Name.equals(c.getString(R.string.asr))) return c.getString(R.string.maghrib);
        if (Name.equals(c.getString(R.string.maghrib))) return c.getString(R.string.isha);
        else return c.getString(R.string.fajr);
    }

    public static void lighScreen(Context context) {
        PowerManager pm = (PowerManager) context.getSystemService(Context.POWER_SERVICE);
        boolean isScreenOn = pm.isScreenOn();
        Log.e("screen on", "" + isScreenOn);
        if (isScreenOn == false) {
            PowerManager.WakeLock wl = pm.newWakeLock(PowerManager.FULL_WAKE_LOCK | PowerManager.ACQUIRE_CAUSES_WAKEUP | PowerManager.ON_AFTER_RELEASE, "MyLock");
            wl.acquire(10000);
            PowerManager.WakeLock wl_cpu = pm.newWakeLock(PowerManager.PARTIAL_WAKE_LOCK, "MyCpuLock");
            wl_cpu.acquire(10000);
        }
    }

    public static void startNextPryerAzan(Context context) {
        Pair<String, Long> pairs[] = getPrevAndNextAzanNameAndTime(context);
        if (pairs == null) return;

        Intent prayedIntent = new Intent(context, AzanSoundBroadcast.class).putExtra(EXTRA_AZNA_NAME, pairs[1].first);
        PendingIntent pendingIntent = PendingIntent.getBroadcast(context, 0, prayedIntent, PendingIntent.FLAG_UPDATE_CURRENT);
        AlarmManager alarmManager = (AlarmManager) context.getSystemService(context.ALARM_SERVICE);

        alarmManager.set(AlarmManager.RTC_WAKEUP, pairs[1].second, pendingIntent);
    }

    private static void startNextAzanWarning(Context context, long date, String name, boolean isWarn, long time) {
        Pair<String, Long> pairs[] = getPrevAndNextAzanNameAndTime(context);
        if (pairs == null) return;

        Intent prayedIntent = new Intent(context, WarningBroadcast.class)
                .putExtra(EXTRA_AZNA_NAME, name)
                .putExtra(EXTRA_IS_WARNING, isWarn)
                .putExtra(EXTRA_TIME, time);
        PendingIntent pendingIntent = PendingIntent.getBroadcast(context, 0, prayedIntent, PendingIntent.FLAG_UPDATE_CURRENT);
        AlarmManager alarmManager = (AlarmManager) context.getSystemService(context.ALARM_SERVICE);

        alarmManager.set(AlarmManager.RTC_WAKEUP, date, pendingIntent);
    }

//    private static void startCurrentAzanCheck(Context context) {
//        Pair<String, Long> pair = getNextAzanNameAndTime(context);
//        Intent prayedIntent = new Intent(context, WarningBroadcast.class)
//                .putExtra(EXTRA_AZNA_NAME, pair.first)
//                .putExtra(EXTRA_IS_WARNING, false);
//        PendingIntent pendingIntent = PendingIntent.getBroadcast(context, 0, prayedIntent, PendingIntent.FLAG_UPDATE_CURRENT);
//        AlarmManager alarmManager = (AlarmManager) context.getSystemService(context.ALARM_SERVICE);
//
//        alarmManager.set(AlarmManager.RTC_WAKEUP, pair.second + PERIOD_AFTER_CHECK, pendingIntent);
//    }

    public static void startAzanNotifications(Context context) {
        Pair<String, Long> pairs[] = getPrevAndNextAzanNameAndTime(context);
        if (pairs == null) return;
        if (pairs[0].second + PERIOD_AFTER_CHECK >= System.currentTimeMillis()) {
            startNextAzanWarning(context, pairs[0].second + PERIOD_AFTER_CHECK, pairs[0].first, false, pairs[0].second);
            Log.d("LOL", "startAzanNotifications: " + 1);
        } else if (pairs[1].second - PERIOD_WARNING_CHECK >= System.currentTimeMillis()) {
            startNextAzanWarning(context, pairs[1].second - PERIOD_WARNING_CHECK, pairs[0].first, true, pairs[0].second);
            Log.d("LOL", "startAzanNotifications: " + 2);

        } else {
            startNextAzanWarning(context, pairs[1].second + PERIOD_AFTER_CHECK, pairs[1].first, false, pairs[1].second);
            Log.d("LOL", "startAzanNotifications: " + 3);

        }

    }


    public static void pushAzanNotification(Context context, String ContentTitle, String ContentText) {
        Intent i = new Intent(context, SalatyActivity.class);
        PendingIntent pendingIntent = PendingIntent.getActivity(context, 1, i, 0);
        Uri uri = Uri.parse("android.resource://com.example.allam.salaty/" + R.raw.adhan2);

        int color = context.getResources().getColor(R.color.colorPrimaryDark);

        Notification.Builder builder = new Notification.Builder(context)
                .setTicker(ContentTitle)
                .setContentTitle(ContentTitle)
                .setContentText(ContentText)
                .setContentIntent(pendingIntent)
                .setSmallIcon(R.drawable.notification)
                .setAutoCancel(true)
                .setSound(uri);

        NotificationManager mNotificationManager =
                (NotificationManager) context.getSystemService(Context.NOTIFICATION_SERVICE);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            String chanel_id = "chanel02";
            builder.setChannelId(chanel_id);
            NotificationChannel mChannel = new NotificationChannel(chanel_id, "Azan", NotificationManager.IMPORTANCE_HIGH);

            // Creating an Audio Attribute
            AudioAttributes audioAttributes = new AudioAttributes.Builder()
                    .setContentType(AudioAttributes.CONTENT_TYPE_SONIFICATION)
                    .setUsage(AudioAttributes.USAGE_ALARM)
                    .build();
            mChannel.setSound(uri, audioAttributes);
            mNotificationManager.createNotificationChannel(mChannel);
        }

        Notification notification = builder.build();
        Utils.lighScreen(context);

        //NotificationManagerCompat NMC = NotificationManagerCompat.from(context);

        mNotificationManager.notify(ContentTitle.hashCode(), notification);
    }


    public static void pushNotification(Context context, String ContentTitle, String ContentText, String azanName, long time) {
        Intent i = new Intent(context, SalatyActivity.class);
        PendingIntent pendingIntent = PendingIntent.getActivity(context, 1, i, 0);
        Uri uri = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);

        int color = context.getResources().getColor(R.color.colorPrimaryDark);

        Intent prayedIntent = new Intent(context, DoneBroadcast.class)
                .putExtra(EXTRA_AZNA_NAME, azanName)
                .putExtra(EXTRA_TIME, time);

        PendingIntent pendingIntentDone = PendingIntent.getBroadcast(context, 0, prayedIntent, PendingIntent.FLAG_UPDATE_CURRENT);

        Notification.Builder builder = new Notification.Builder(context)
                .setTicker(ContentTitle)
                .setContentTitle(ContentTitle)
                .setContentText(ContentText)
                .setContentIntent(pendingIntent)
                .setSmallIcon(R.drawable.notification)
                .setSound(uri)
                .setAutoCancel(true)
                .addAction(R.drawable.ic_done, "Done", pendingIntentDone)
                .setPriority(Notification.PRIORITY_MAX)
                .setStyle(new Notification.BigTextStyle().bigText(ContentText));

        NotificationManager mNotificationManager =
                (NotificationManager) context.getSystemService(Context.NOTIFICATION_SERVICE);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            String chanel_id = "chanel03";
            builder.setChannelId(chanel_id);
            NotificationChannel mChannel = new NotificationChannel(chanel_id, "Azan", NotificationManager.IMPORTANCE_HIGH);

            // Creating an Audio Attribute
            AudioAttributes audioAttributes = new AudioAttributes.Builder()
                    .setContentType(AudioAttributes.CONTENT_TYPE_SONIFICATION)
                    .setUsage(AudioAttributes.USAGE_ALARM)
                    .build();
            mChannel.setSound(uri, audioAttributes);
            mNotificationManager.createNotificationChannel(mChannel);
        }

        Utils.lighScreen(context);

        Notification notification = builder.build();
        notification.flags |= Notification.FLAG_SHOW_LIGHTS;

        mNotificationManager.notify(azanName.hashCode(), notification);
    }


    public static void startPollService(Context context) {
        if (!PollDateService.isServiceAlarmOn(context))
            PollDateService.setServiceAlarm(context);
    }


    public static String getToday() {
        Calendar cal = Calendar.getInstance();
        cal.setTime(new Date());
        return Utils.SIMPLE_DATE_FORMAT_DATABASE_QUERY.format(cal.getTime());
    }
}
