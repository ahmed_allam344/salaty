package com.example.allam.salaty.service;

import android.app.AlarmManager;
import android.app.IntentService;
import android.app.Notification;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.net.ConnectivityManager;
import android.os.SystemClock;
import android.support.v4.app.NotificationCompat;
import android.support.v4.app.NotificationManagerCompat;
import android.util.Log;
import android.widget.Toast;

import com.example.allam.salaty.activity.SalatyActivity;
import com.example.allam.salaty.network.AdhanFetcher;
import com.example.allam.salaty.utils.Utils;

import java.io.IOException;

/**
 * Created by shawara on 1/27/2017.
 */

public class PollDateService extends IntentService {
    private static final String TAG = "PollService";
    private static final int POLL_INTERVAL = 1000 * 60 * 60 * 8; // 8 hours

    public PollDateService() {
        super(TAG);
    }

    public static Intent newIntent(Context context) {
        return new Intent(context, PollDateService.class);
    }


    @Override
    protected void onHandleIntent(Intent intent) {
        if (!Utils.isNetworkAvailableAndConnected(this)) {
            return;
        }

        try {
            new AdhanFetcher(getBaseContext()).FetchAthanData();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public static boolean isServiceAlarmOn(Context context) {
        Intent i = PollDateService.newIntent(context);
        PendingIntent pi = PendingIntent.getService(context, 0, i, PendingIntent.FLAG_NO_CREATE);
        return pi != null;
    }

    public static void setServiceAlarm(Context context) {
        Intent i = PollDateService.newIntent(context);
        PendingIntent pi = PendingIntent.getService(context, 0, i, 0);
        AlarmManager alarmManager = (AlarmManager) context.getSystemService(Context.ALARM_SERVICE);
        alarmManager.setInexactRepeating(AlarmManager.ELAPSED_REALTIME, SystemClock.elapsedRealtime(), POLL_INTERVAL, pi);
    }

}
