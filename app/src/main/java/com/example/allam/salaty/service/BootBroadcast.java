package com.example.allam.salaty.service;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;

import com.example.allam.salaty.utils.Utils;

/**
 * Created by Allam on 2/11/2017.
 */

public class BootBroadcast extends BroadcastReceiver {

    @Override
    public void onReceive(Context context, Intent intent) {
        //AdhanSoundService.setAdhanAlarm(context, true);
        Utils.startPollService(context);
        Utils.startNextPryerAzan(context);
        Utils.startAzanNotifications(context);
    }
}
