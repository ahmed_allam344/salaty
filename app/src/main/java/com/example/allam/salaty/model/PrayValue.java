package com.example.allam.salaty.model;

/**
 * Created by Allam on 1/6/2017.
 */

public class PrayValue {

    public PrayValue() {
    }

    private int Fajr;
    private int Sunrise;
    private int Dhuhr;
    private int Asr;
    private int Sunset;
    private int Maghrib;
    private int Isha;
    private int Imsak;
    private int Midnight;
    private String Date;

    public int getFajr() {
        return Fajr;
    }

    public void setFajr(int fajr) {
        Fajr = fajr;
    }

    public int getSunrise() {
        return Sunrise;
    }

    public void setSunrise(int sunrise) {
        Sunrise = sunrise;
    }

    public int getDhuhr() {
        return Dhuhr;
    }

    public void setDhuhr(int dhuhr) {
        Dhuhr = dhuhr;
    }

    public int getAsr() {
        return Asr;
    }

    public void setAsr(int asr) {
        Asr = asr;
    }

    public int getSunset() {
        return Sunset;
    }

    public void setSunset(int sunset) {
        Sunset = sunset;
    }

    public int getMaghrib() {
        return Maghrib;
    }

    public void setMaghrib(int maghrib) {
        Maghrib = maghrib;
    }

    public int getIsha() {
        return Isha;
    }

    public void setIsha(int isha) {
        Isha = isha;
    }

    public int getImsak() {
        return Imsak;
    }

    public void setImsak(int imsak) {
        Imsak = imsak;
    }

    public int getMidnight() {
        return Midnight;
    }

    public void setMidnight(int midnight) {
        Midnight = midnight;
    }

    public String getDate() {
        return Date;
    }

    public void setDate(String date) {
        Date = date;
    }
}
