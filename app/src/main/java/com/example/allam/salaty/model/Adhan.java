package com.example.allam.salaty.model;

import java.util.List;

/**
 * Created by Allam on 12/12/2016.
 */
public class Adhan {

    /**
     * timings : {"Fajr":"05:02 (EET)","Sunrise":"06:34 (EET)","Dhuhr":"11:44 (EET)","Asr":"14:36 (EET)","Sunset":"16:55 (EET)","Maghrib":"16:55 (EET)","Isha":"18:17 (EET)","Imsak":"04:52 (EET)","Midnight":"23:44 (EET)"}
     * date : {"readable":"01 Dec 2016","timestamp":"1480575661"}
     */

    private List<AdhanData> data;

    public List<AdhanData> getData() {
        return data;
    }

    public void setData(List<AdhanData> data) {
        this.data = data;
    }

    public static class AdhanData {
        /**
         * Fajr : 05:02 (EET)
         * Sunrise : 06:34 (EET)
         * Dhuhr : 11:44 (EET)
         * Asr : 14:36 (EET)
         * Sunset : 16:55 (EET)
         * Maghrib : 16:55 (EET)
         * Isha : 18:17 (EET)
         * Imsak : 04:52 (EET)
         * Midnight : 23:44 (EET)
         */

        private TimingsBean timings;
        /**
         * readable : 01 Dec 2016
         * timestamp : 1480575661
         */

        private DateBean date;

        public TimingsBean getTimings() {
            return timings;
        }

        public void setTimings(TimingsBean timings) {
            this.timings = timings;
        }

        public DateBean getDate() {
            return date;
        }

        public void setDate(DateBean date) {
            this.date = date;
        }

        public static class TimingsBean {
            private String Fajr;
            private String Sunrise;
            private String Dhuhr;
            private String Asr;
            private String Sunset;
            private String Maghrib;
            private String Isha;
            private String Imsak;
            private String Midnight;

            public String getFajr() {
                return Fajr;
            }

            public void setFajr(String Fajr) {
                this.Fajr = Fajr;
            }

            public String getSunrise() {
                return Sunrise;
            }

            public void setSunrise(String Sunrise) {
                this.Sunrise = Sunrise;
            }

            public String getDhuhr() {
                return Dhuhr;
            }

            public void setDhuhr(String Dhuhr) {
                this.Dhuhr = Dhuhr;
            }

            public String getAsr() {
                return Asr;
            }

            public void setAsr(String Asr) {
                this.Asr = Asr;
            }

            public String getSunset() {
                return Sunset;
            }

            public void setSunset(String Sunset) {
                this.Sunset = Sunset;
            }

            public String getMaghrib() {
                return Maghrib;
            }

            public void setMaghrib(String Maghrib) {
                this.Maghrib = Maghrib;
            }

            public String getIsha() {
                return Isha;
            }

            public void setIsha(String Isha) {
                this.Isha = Isha;
            }

            public String getImsak() {
                return Imsak;
            }

            public void setImsak(String Imsak) {
                this.Imsak = Imsak;
            }

            public String getMidnight() {
                return Midnight;
            }

            public void setMidnight(String Midnight) {
                this.Midnight = Midnight;
            }
        }

        public static class DateBean {
            private String readable;
            private String timestamp;

            public String getReadable() {
                return readable;
            }

            public void setReadable(String readable) {
                this.readable = readable;
            }

            public String getTimestamp() {
                return timestamp;
            }

            public void setTimestamp(String timestamp) {
                this.timestamp = timestamp;
            }
        }
    }
}
