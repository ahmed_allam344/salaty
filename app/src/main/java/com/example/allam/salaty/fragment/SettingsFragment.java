package com.example.allam.salaty.fragment;

import android.os.Bundle;
import android.preference.Preference;
import android.preference.PreferenceFragment;
import android.preference.PreferenceManager;

import com.example.allam.salaty.R;

/**
 * Created by Allam on 1/30/2017.
 */

public class SettingsFragment extends PreferenceFragment implements Preference.OnPreferenceChangeListener {

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        addPreferencesFromResource(R.xml.preferences);
        bindPrenferenceSummarytoValue(findPreference(getString(R.string.pref_method_key)));
        bindPrenferenceSummarytoValue(findPreference(getString(R.string.pref_city_key)));
        bindPrenferenceSummarytoValue(findPreference(getString(R.string.pref_country_key)));
        bindPrenferenceSummarytoValue(findPreference(getString(R.string.pref_school_key)));
    }

    public void bindPrenferenceSummarytoValue(Preference preference) {
        preference.setOnPreferenceChangeListener(this);
        onPreferenceChange(preference,
                PreferenceManager.getDefaultSharedPreferences(preference.getContext()).getString(preference.getKey(), ""));

    }

    @Override
    public boolean onPreferenceChange(Preference preference, Object newValue) {
        String value = newValue.toString();
        if (preference.getKey().equals(getString(R.string.pref_method_key))) {
            if (value.equals("1")) {
                preference.setSummary(R.string.method1);
            } else if (value.equals("2")) {
                preference.setSummary(R.string.method2);
            } else if (value.equals("3")) {
                preference.setSummary(R.string.method3);
            } else if (value.equals("4")) {
                preference.setSummary(R.string.method4);
            } else if (value.equals("5")) {
                preference.setSummary(R.string.method5);
            } else if (value.equals("7")) {
                preference.setSummary(R.string.method7);
            }

        } else if (preference.getKey().equals(getString(R.string.pref_school_key))) {
            if (value.equals("0")) {
                preference.setSummary("Shafii");
            } else {
                preference.setSummary("Hanfi");
            }
        }else {
            preference.setSummary(value);
        }
        //getActivity().finish();
        return true;
    }
}
