package com.example.allam.salaty.activity;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.NavigationView;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBar;
import android.text.Html;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.CalendarView;

import com.example.allam.salaty.fragment.AdhanFragment;
import com.example.allam.salaty.R;
import com.example.allam.salaty.utils.Utils;

import java.util.Calendar;
import java.util.Date;

public class SalatyActivity extends SingleFragmentActivity {
    private DrawerLayout mDrawerLayout;
    private CalendarView mCalendar;


    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);


        Utils.startPollService(getBaseContext());
        Utils.startNextPryerAzan(this);
        Utils.startAzanNotifications(this);

        final ActionBar actionBar = getSupportActionBar();
        actionBar.setTitle(Html.fromHtml("<font color='#534c49'>" + Utils.SIMPLE_DATE_FORMAT.format(new Date()) + "</font>"));
        actionBar.setHomeAsUpIndicator(R.drawable.ic_menu);
        actionBar.setDisplayHomeAsUpEnabled(true);
        actionBar.setElevation(4);

        mDrawerLayout = (DrawerLayout) findViewById(R.id.drawer_layout);
        NavigationView navigationView = (NavigationView) findViewById(R.id.nav_view);


        View headerView = navigationView.getHeaderView(0);

        mCalendar = (CalendarView) headerView.findViewById(R.id.nav_calender);

        mCalendar.setOnDateChangeListener(new CalendarView.OnDateChangeListener() {
            @Override
            public void onSelectedDayChange(CalendarView view, int year, int month, int dayOfMonth) {
                mDrawerLayout.closeDrawers();

                Calendar cal = Calendar.getInstance();
                cal.set(year, month, dayOfMonth);
                String dayDate = Utils.SIMPLE_DATE_FORMAT_DATABASE_QUERY.format(cal.getTime());
                replaceFragment(dayDate);
                actionBar.setTitle(Html.fromHtml("<font color='#534c49'>" + Utils.SIMPLE_DATE_FORMAT.format(cal.getTime()) + "</font>"));
            }
        });
    }


    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        switch (id) {
            case android.R.id.home:
                mDrawerLayout.openDrawer(GravityCompat.START);
                return true;
            case R.id.settings_menu_id:
                startActivity(new Intent(getBaseContext(), SettingsActivity.class));
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }


    @Override
    public Fragment getFragment() {
        return AdhanFragment.newInstance(Utils.getToday());
    }

    @Override
    public int getLayoutID() {
        return R.layout.activity_salaty;
    }

    @Override
    public int getContainerID() {
        return R.id.athan_container;
    }


    private void replaceFragment(String date) {
        FragmentManager fm = getSupportFragmentManager();
        fm.beginTransaction().replace(R.id.athan_container, AdhanFragment.newInstance(date)).commit();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.preference_menu, menu);
        return true;
    }


}


//    private void setupDrawerContent(NavigationView navigationView) {
//        navigationView.setNavigationItemSelectedListener(
//                new NavigationView.OnNavigationItemSelectedListener() {
//                    @Override
//                    public boolean onNavigationItemSelected(MenuItem menuItem) {
//                        menuItem.setChecked(true);
//                        mDrawerLayout.closeDrawers();
//                        return true;
//                    }
//                });
//    }
