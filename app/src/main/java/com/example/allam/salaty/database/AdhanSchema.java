package com.example.allam.salaty.database;

import android.provider.BaseColumns;

/**
 * Created by Allam on 1/4/2017.
 */
public class AdhanSchema {
    public static String DATABASE_NAME = "athantimes.db";

    public static class AdhanTable {
        public static String TABLE_NAME = "athan_table";

        public static class Columns implements BaseColumns {
            public static String FAGR = "fagr";
            public static String SUNRISE = "sunrise";
            public static String DHUHR = "dhuhr";
            public static String ASR = "asr";
            public static String MAGHRIB = "maghrib";
            public static String ISHA = "isha";
            public static String IMSAK = "imsak";
            public static String MIDNIGHT = "midnight";
            public static String DATE = "datestamp";
            public static String TIMESTAMP = "timestamp";
        }

    }

    public static class PrayValueTable {
        public static String TABLE_NAME = "pray_value_table";

        public static class Columns implements BaseColumns {
            public static String FAGR_VALUE = "fagr_value";
            public static String SUNRISE_VALUE = "sunrise_value";
            public static String DHUHR_VALUE = "dhuhr_value";
            public static String ASR_VALUE = "asr_value";
            public static String MAGHRIB_VALUE = "maghrib_value";
            public static String ISHA_VALUE = "isha_value";
            public static String IMSAK_VALUE = "imsak_value";
            public static String MIDNIGHT_VALUE = "midnight_value";
            public static String DATE_VALUE = "datestamp";
        }

    }
}
