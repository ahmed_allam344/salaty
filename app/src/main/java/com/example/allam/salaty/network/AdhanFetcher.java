package com.example.allam.salaty.network;

import android.content.Context;
import android.net.Uri;
import android.util.Log;
import android.widget.Toast;

import com.example.allam.salaty.activity.SalatyActivity;
import com.example.allam.salaty.database.DatabaseQueries;
import com.example.allam.salaty.model.Adhan;
import com.example.allam.salaty.utils.Utils;
import com.google.gson.Gson;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.List;


/**
 * Created by Allam on 12/12/2016.
 */
public class AdhanFetcher {
    private Context mContext;

    public AdhanFetcher(Context context) {
        mContext = context;
    }

    //each item will be handled with chooses options in settings later
    private static final String URL = "http://api.aladhan.com"; // Url.com
    private static final String Calender = "calendar"; // Url.com/Endpoint
    private static final String Latitude = "30.0444196"; //latitude For Cairo
    private static final String Longitude = "31.2357116";//longitude For Cairo
    private static final String TimeZone = "Africa/Cairo";
    private static final String Method = "5"; //Egyptian General Authority of Survey
    private static final String school = "0";//shaf3i school

    private static String getCalenderUrl() {
        Uri uri = Uri.parse(URL).buildUpon().appendPath(Calender)
                .appendQueryParameter("latitude", Latitude)
                .appendQueryParameter("longitude", Longitude)
                .appendQueryParameter("timezonestring", TimeZone)
                .appendQueryParameter("method", Method).build();
        Log.i("ATHAN", uri.toString());
        return uri.toString();
    }


    private byte[] getUrlBytes(String ApiUrl) throws IOException {
        URL url = new URL(ApiUrl);
        HttpURLConnection connection = (HttpURLConnection) url.openConnection();
        try {
            ByteArrayOutputStream out = new ByteArrayOutputStream();
            InputStream in = connection.getInputStream();
            if (connection.getResponseCode() != HttpURLConnection.HTTP_OK) {
                Toast.makeText(mContext, "Check Internet Connection!", Toast.LENGTH_LONG).show();
            }
            int bytesRead;
            byte[] buffer = new byte[1024];
            while ((bytesRead = in.read(buffer)) > 0) {
                out.write(buffer, 0, bytesRead);
            }
            out.close();
            return out.toByteArray();
        } finally {
            connection.disconnect();
        }

    }

    private String getUrlString(String urlSpec) throws IOException {
        return new String(getUrlBytes(urlSpec));
    }

    public List<Adhan.AdhanData> FetchAthanData() throws IOException {

        Gson gson = new Gson();
        String urlString = getUrlString(getCalenderUrl());
        Adhan adhan = gson.fromJson(urlString, Adhan.class);
        long count = insertNewPrayTimes(adhan.getData());
        Log.i(Utils.LOGTAG, count + " rows inserted successfully");
        return adhan.getData();
    }

    public long insertNewPrayTimes(List<Adhan.AdhanData> list) {
        long count = 0;
        for (int i = 0; i < list.size(); i++) {
            count = new DatabaseQueries(mContext).insertAdhanTimes(list.get(i));
        }
        return count;
    }


}
