package com.example.allam.salaty.activity;

import android.os.Bundle;
import android.support.annotation.IdRes;
import android.support.annotation.LayoutRes;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v7.app.AppCompatActivity;

/**
 * Created by Allam on 12/11/2016.
 */
public abstract class SingleFragmentActivity extends AppCompatActivity {


    public abstract Fragment getFragment();
    @LayoutRes
    public abstract int getLayoutID();
    @IdRes
    public abstract int getContainerID();



    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(getLayoutID());
        FragmentManager fm = getSupportFragmentManager();
        Fragment fragment = fm.findFragmentById(getContainerID());
        if(fragment == null){
            fragment = getFragment();
            fm.beginTransaction().add(getContainerID(), fragment).commit();
        }
    }
}
