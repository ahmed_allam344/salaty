package com.example.allam.salaty.database;

import android.database.Cursor;
import android.database.CursorWrapper;

import com.example.allam.salaty.model.Adhan.AdhanData;
import com.example.allam.salaty.model.Adhan.AdhanData.DateBean;
import com.example.allam.salaty.model.Adhan.AdhanData.TimingsBean;
import com.example.allam.salaty.model.PrayValue;

import static com.example.allam.salaty.database.AdhanSchema.AdhanTable.Columns.*;
import static com.example.allam.salaty.database.AdhanSchema.PrayValueTable.Columns.*;


/**
 * Created by Allam on 1/6/2017.
 */

public class DataCursorWraper extends CursorWrapper {
    /**
     * Creates a cursor wrapper.
     *
     * @param cursor The underlying cursor to wrap.
     */
    public DataCursorWraper(Cursor cursor) {
        super(cursor);
    }

    public  AdhanData getAdhanCursorData() {
        String fagr = getString(getColumnIndex(FAGR));
        String sunrise = getString(getColumnIndex(SUNRISE));
        String dhuhr = getString(getColumnIndex(DHUHR));
        String asr = getString(getColumnIndex(ASR));
        String maghrib = getString(getColumnIndex(MAGHRIB));
        String isha = getString(getColumnIndex(ISHA));
        String imsak =getString(getColumnIndex(IMSAK));
        String midnight = getString(getColumnIndex(MIDNIGHT));
        String date = getString(getColumnIndex(DATE));
        String timestamp = getString(getColumnIndex(TIMESTAMP));

        AdhanData adhan = new AdhanData();
        TimingsBean timings = new TimingsBean();
        DateBean dateBean = new DateBean();

        timings.setFajr(fagr);
        timings.setSunrise(sunrise);
        timings.setDhuhr(dhuhr);
        timings.setAsr(asr);
        timings.setMaghrib(maghrib);
        timings.setIsha(isha);
        timings.setImsak(imsak);
        timings.setMidnight(midnight);

        dateBean.setReadable(date);
        dateBean.setTimestamp(timestamp);

        adhan.setTimings(timings);
        adhan.setDate(dateBean);
        return adhan;
    }

    public PrayValue getPrayValuesCursorData() {
        int fagr = getInt(getColumnIndex(FAGR_VALUE));
        int sunrise = getInt(getColumnIndex(SUNRISE_VALUE));
        int dhuhr = getInt(getColumnIndex(DHUHR_VALUE));
        int asr = getInt(getColumnIndex(ASR_VALUE));
        int maghrib = getInt(getColumnIndex(MAGHRIB_VALUE));
        int isha = getInt(getColumnIndex(ISHA_VALUE));
        int imsak =getInt(getColumnIndex(IMSAK_VALUE));
        int midnight = getInt(getColumnIndex(MIDNIGHT_VALUE));
        String date = getString(getColumnIndex(DATE_VALUE));

        PrayValue prayValue = new PrayValue();

        prayValue.setFajr(fagr);
        prayValue.setSunrise(sunrise);
        prayValue.setDhuhr(dhuhr);
        prayValue.setAsr(asr);
        prayValue.setMaghrib(maghrib);
        prayValue.setIsha(isha);
        prayValue.setImsak(imsak);
        prayValue.setMidnight(midnight);
        prayValue.setDate(date);

        return prayValue;
    }
}
