package com.example.allam.salaty.service;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.support.v4.app.NotificationManagerCompat;
import android.util.Log;
import android.widget.Toast;

import com.example.allam.salaty.utils.Utils;

/**
 * Created by shawara on 2/14/2017.
 */

public class DoneBroadcast extends BroadcastReceiver {
    @Override
    public void onReceive(Context context, Intent intent) {
        String azanName = intent.getStringExtra(Utils.EXTRA_AZNA_NAME);
        long time = intent.getLongExtra(Utils.EXTRA_TIME, System.currentTimeMillis());
        Utils.update(azanName, context, time);

        Toast.makeText(context, azanName + " is marked in time successfully.", Toast.LENGTH_SHORT).show();
        NotificationManagerCompat NMC = NotificationManagerCompat.from(context);
        NMC.cancel(azanName.hashCode());

    }
}
