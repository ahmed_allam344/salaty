package com.example.allam.salaty.activity;

import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.example.allam.salaty.R;
import com.example.allam.salaty.database.DatabaseQueries;
import com.example.allam.salaty.model.Adhan.AdhanData;
import com.example.allam.salaty.network.AdhanFetcher;
import com.example.allam.salaty.utils.Utils;

import java.io.IOException;
import java.util.List;

/**
 * Created by Allam on 2/15/2017.
 */

public class SplashActivity extends AppCompatActivity {
    private String mCurrentDate;
    private DatabaseQueries mDatabaseQueries;
    private AdhanData adhanData;
    private Button mDone;
    private ImageView mWarningImage;
    private TextView mSplashText;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.splash_layout);

        mDone = (Button) findViewById(R.id.splash_button);
        mWarningImage = (ImageView) findViewById(R.id.warnning_image);
        mSplashText = (TextView) findViewById(R.id.splash_text);

        mCurrentDate = Utils.getToday();
        mDatabaseQueries = new DatabaseQueries(this);
        adhanData = mDatabaseQueries.getPrayTimesAtDay(mCurrentDate);



        if (adhanData != null) {
            Intent intent = new Intent(this, SalatyActivity.class);
            startActivity(intent);
            finish();
        } else {
            if (!Utils.isNetworkAvailableAndConnected(getApplicationContext())) {
                mSplashText.setText(getString(R.string.no_connection_splash_text));
                mDone.setVisibility(View.VISIBLE);
                mWarningImage.setVisibility(View.VISIBLE);
                mDone.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        if (Utils.isNetworkAvailableAndConnected(getApplicationContext())) {
                            new getPrayValuesAsyncTask().execute();
                            mSplashText.setText(getString(R.string.waiting));
                            mSplashText.setTextSize(32);
                            mWarningImage.setVisibility(View.INVISIBLE);
                            mDone.setVisibility(View.INVISIBLE);
                        }else {
                            Toast.makeText(SplashActivity.this, getString(R.string.no_connection), Toast.LENGTH_SHORT).show();
                        }
                    }
                });
            } else {
                new getPrayValuesAsyncTask().execute();
            }
        }


    }

    private class getPrayValuesAsyncTask extends AsyncTask<Void, Void, List<AdhanData>> {

        @Override
        protected List<AdhanData> doInBackground(Void... params) {
            try {
                return new AdhanFetcher(getBaseContext()).FetchAthanData();
            } catch (IOException e) {
                e.printStackTrace();
            }
            return null;
        }

        @Override
        protected void onPostExecute(List<AdhanData> data) {
            super.onPostExecute(data);

            if (data != null) {
                Intent intent = new Intent(getBaseContext(), SalatyActivity.class);
                intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK|Intent.FLAG_ACTIVITY_NEW_TASK);
                startActivity(intent);
            }
        }
    }
}
